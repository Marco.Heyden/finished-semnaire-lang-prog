#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#define SIZE 1024

void write_file(int sockfd){
  int n;
  FILE *fp;
  char *filename = "recv.txt";
  char buffer[SIZE];

  fp = fopen(filename, "w");
  while (1) {
    n = recv(sockfd, buffer, SIZE, 0);
    if (n <= 0){
      break;
      return;
    }
    fprintf(fp, "%s", buffer);
    bzero(buffer, SIZE);
  }
  return;
}







int main(void)
{
    int socket_desc;
    struct sockaddr_in server_addr;
    char server_message[2048], client_message[2048], client_message2[2048], server_message2[2048];
    char exit[] = "exit";

    char list[] = "list";

    // Clean buffers:
    memset(server_message,'\0',sizeof(server_message));
    memset(client_message,'\0',sizeof(client_message));

    // Create socket:
    socket_desc = socket(AF_INET, SOCK_STREAM, 0);

    if(socket_desc < 0){
        printf("Unable to create socket\n");
        return -1;
    }

    printf("Socket created successfully\n");

    // Set port and IP the same as server-side:
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(5000);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    // Send connection request to server:
    if(connect(socket_desc, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0){
        printf("Unable to connect\n");
        return -1;
    }
    printf("Connected with server successfully\n");

   while(1){
    // Get input from the user:
    printf("Enter command:(list,  get  <filename>, exit)");
    gets(client_message);






    if (strcmp(client_message,exit)==0){
        printf("the user wants to exit the program\n");
                if(send(socket_desc, client_message, strlen(client_message), 0) < 0){
                printf("Unable to send message\n");
                return -1;
    }
    break;
    }
    else{

    // Send the message to server:
    if(send(socket_desc, client_message, strlen(client_message), 0) < 0){
        printf("Unable to send message\n");
        return -1;
    }

    // Receive the server's response:
    if(recv(socket_desc, server_message, sizeof(server_message), 0) < 0){
        printf("Error while receiving server's msg\n");
        return -1;
    }


    printf("Server's response:\n%s",server_message);
    memset(client_message, '\0', sizeof client_message);
    memset(server_message, '\0', sizeof server_message);

    }

    }

    // Close the socket:
    close(socket_desc);

    return 0;
}
